<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Permission;

class InsertPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = [

            [
                "identifier" => "admin.home.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "الرئيسية"],
                "en" => ["name" => "home"],
            ],
            [
                "identifier" => "admin.routes.index",
                "active" => true,
                "type" => 0,
                "ar" => [ "name" => "عرض الطرق" ],
                "en" => [ "name" => "routes index" ],
            ],
            [
                "identifier" => "admin.routes.create",
                "active" => true,
                "type" => 0,
                "ar" => [ "name" => " اضافة طريق" ],
                "en" => [ "name" => "route create" ],
            ],
            [
                "identifier" => "admin.routes.destroy",
                "active" => true,
                "type" => 0,
                "ar" => [ "name" => " اضافة طريق" ],
                "en" => [ "name" => "route delete" ],
            ],
            [
                "identifier" => "admin.routes.update",
                "active" => true,
                "type" => 0,
                "ar" => [ "name" => "تغديل الطريق" ],
                "en" => [ "name" => "route update" ],
            ],
            [
                "identifier" => "admin.categories.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض المنتجات"],
                "en" => ["name" => "list of categories"],
            ],
            [
                "identifier" => "admin.categories.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء فئة جديده"],
                "en" => ["name" => "create new category"],
            ],
            [
                "identifier" => "admin.categories.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل الفئة"],
                "en" => ["name" => "update of category"],
            ],
            [
                "identifier" => "admin.categories.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف الفئة"],
                "en" => ["name" => "delete of category"],
            ],
            [
                "identifier" => "admin.categories.show",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "رؤية الأقسام الفرعيه"],
                "en" => ["name" => "show sub of category"],
            ],
            [
                "identifier" => "admin.groups.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض المجموعات"],
                "en" => ["name" => "list of groups"],
            ],
            [
                "identifier" => "admin.groups.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء مجموعة جديده"],
                "en" => ["name" => "create new group"],
            ],
            [
                "identifier" => "admin.groups.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل المجموعة"],
                "en" => ["name" => "update of group"],
            ],
            [
                "identifier" => "admin.groups.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف المجموعة"],
                "en" => ["name" => "delete of group"],
            ],
            [
                "identifier" => "admin.group.permissions.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض مجموعة الصلاحيات"],
                "en" => ["name" => "list of group permissions"],
            ],
            [
                "identifier" => "admin.group.permissions.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء مجموعة الصلاحيات جديده"],
                "en" => ["name" => "create new group permissions"],
            ],
            [
                "identifier" => "admin.group.permissions.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل مجموعة الصلاحيات"],
                "en" => ["name" => "update of group permissions"],
            ],
            [
                "identifier" => "admin.group.permissions.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف مجموعة الصلاحيات"],
                "en" => ["name" => "delete of group permissions"],
            ],
            [
                "identifier" => "admin.locations.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض اﻷماكن"],
                "en" => ["name" => "list of locations"],
            ],
            [
                "identifier" => "admin.locations.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء مكان جديد"],
                "en" => ["name" => "create new location"],
            ],
            [
                "identifier" => "admin.locations.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل المكان"],
                "en" => ["name" => "update of location"],
            ],
            [
                "identifier" => "admin.locations.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف المكان"],
                "en" => ["name" => "delete of location"],
            ],
            [
                "identifier" => "admin.orders.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض الطلبات"],
                "en" => ["name" => "list of orders"],
            ],
            [
                "identifier" => "admin.orders.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء طلب جديد"],
                "en" => ["name" => "create new order"],
            ],
            [
                "identifier" => "admin.orders.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل الطلب"],
                "en" => ["name" => "update of order"],
            ],
            [
                "identifier" => "admin.orders.show",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تفاصيل الطلبات"],
                "en" => ["name" => "orders details"],
            ],
            [
                "identifier" => "admin.orders.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف الطلب"],
                "en" => ["name" => "delete of order"],
            ],
            [
                "identifier" => "admin.order.products",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض منتجات الطلب"],
                "en" => ["name" => "list of order products"],
            ],
            [
                "identifier" => "admin.order.products.add",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء منتج طلب جديد"],
                "en" => ["name" => "create new order products"],
            ],
            [
                "identifier" => "admin.order.products.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل منتج طلب"],
                "en" => ["name" => "update of order products"],
            ],
            [
                "identifier" => "admin.order.products.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف منتج طلب"],
                "en" => ["name" => "delete of order products"],
            ],
            [
                "identifier" => "admin.permissions.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض الصلحيات"],
                "en" => ["name" => "list of permissions"],
            ],
            [
                "identifier" => "admin.permissions.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل الصالحية"],
                "en" => ["name" => "update of permission"],
            ],
            [
                "identifier" => "admin.products.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض المنتجات"],
                "en" => ["name" => "list of products"],
            ],
            [
                "identifier" => "admin.products.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء منتج جديد"],
                "en" => ["name" => "create new product"],
            ],
            [
                "identifier" => "admin.products.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل المنتج"],
                "en" => ["name" => "update of product"],
            ],
            [
                "identifier" => "admin.products.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف المنتج"],
                "en" => ["name" => "delete of product"],
            ],
            [
                "identifier" => "admin.product.image.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف صوره المنتج"],
                "en" => ["name" => "delete of product image"],
            ],
            [
                "identifier" => "admin.users.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض المستخدمين"],
                "en" => ["name" => "list of users"],
            ],
            [
                "identifier" => "admin.users.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء مستخدم جديد"],
                "en" => ["name" => "create new user"],
            ],
            [
                "identifier" => "admin.users.update",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل المستخدم"],
                "en" => ["name" => "update of user"],
            ],
            [
                "identifier" => "admin.users.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف المستخدم"],
                "en" => ["name" => "delete of user"],
            ],
           
            [
                "identifier" => "admin.user.devices.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "عرض أجهزة المستخدم"],
                "en" => ["name" => "list of user devices"],
            ],
            [
                "identifier" => "admin.user.devices.create",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "انشاء جهاز مستخدم جديد"],
                "en" => ["name" => "create new user device"],
            ],
            [
                "identifier" => "admin.user.devices.destroy",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "حذف جهاز المستخدم"],
                "en" => ["name" => "delete of user device"],
            ],
            [
                "identifier" => "admin.users.groups.index",
                "active" => true,
                "type" => 0,
                "ar" => ["name" => "تعديل مجموعات المستخدم"],
                "en" => ["name" => "update user groups"],
            ],
            
        ];

        foreach ($permissions as $permission) {
            $permissionObj = new Permission($permission);
            $permissionObj->identifier = $permission['identifier'];
            $permissionObj->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
