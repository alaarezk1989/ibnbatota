<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('routes_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('route_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->unique(['route_id','locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
        Schema::dropIfExists('routes_translations');
    }
}
