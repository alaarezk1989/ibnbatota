<?php

    use App\Models\Route;
    use Faker\Factory;
    use Illuminate\Database\Seeder;

class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = factory( Route::class, 5)->create();
    }
}
