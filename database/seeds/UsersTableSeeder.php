<?php

use App\Constants\UserTypes;
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = factory(User::class)->create([
            'name' => 'Super',
            'email' => 'admin@admin.com',
            'password' =>'102030',
            'status' => 1,
            'type' => UserTypes::ADMIN
        ]);
        $user->groups()->attach(1);
        factory(User::class, 5)->create();
    }

}
