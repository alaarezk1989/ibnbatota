<?php

use App\Models\Location;
use Faker\Factory;
use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
   
    public function run()
    {
        $locations = factory( Location::class, 5)->create();
    }
}
