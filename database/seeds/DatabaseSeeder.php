<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(GroupsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ProductsSeeder::class);
        $this->call(OrdersSeeder::class);
        $this->call(RoutesSeeder::class);
        $this->call(LocationsSeeder::class);
        
        $this->call(OrderProductsSeeder::class);
        //$this->call(LogsSeeder::class);
        $this->call(GroupPermissionsSeeder::class);
    }
}
