<?php

use App\Models\Route;
use Faker\Generator as Faker;
use Faker\Factory;

$factory->define( Route::class, function (Faker $faker) {

    $routes = [
        'active' => $faker->boolean,
    ];
$arabicFaker = Factory::create('ar_SA');

    foreach (Config::get('app.locales') as $lang => $language) {
        $tempFaker = $lang == 'ar' ? $arabicFaker : $faker;
        $routes[$lang] = [
            "title" => $tempFaker->text(20),
           ];
    }

    return $routes;
});


