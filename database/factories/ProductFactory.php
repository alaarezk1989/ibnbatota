<?php

use Faker\Generator as Faker;
use App\Models\Category ;
use App\Models\Product ;
use Faker\Factory;

$factory->define(Product::class, function (Faker $faker) {
    $categories = Category::all() ;
    $arabicFaker = Factory::create("ar_SA");
    $product = [
        "price" => 15 ,
        "discount" => 30 ,
        "active" => 1 ,
        "code" =>  "M21" ,
        "image" =>  $faker->imageUrl() ,
        "category_id" => 1
    ];

    foreach (Config::get('app.locales') as $lang => $language) {
        $tempFaker = $lang == 'ar' ? $arabicFaker : $faker;
        $product[$lang] = [
            "name" => $tempFaker->text(20),
            "description" => $tempFaker->text(250),
    ];
    }

    return $product ;
});
