<?php

use App\Models\Category;
use App\Models\OrderProduct;
use App\Models\Order;
use App\Models\BranchProduct;
use App\Models\Product;
use Faker\Factory;
use Faker\Generator as Faker;

$factory->define(OrderProduct::class, function (Faker $faker) {

    $orders = Order::all();
    $products = Product::all();
    $categories = Category::all();

    $orderProduct = [
        'order_id' => $faker->randomElement($orders)->id,
        'product_id' => $faker->randomElement($products)->id,
        "quantity" => $faker->randomDigit,
        "price" => $faker->randomDigit,
        "total" => $faker->randomDigit,
    ];


    
    return $orderProduct;
});
