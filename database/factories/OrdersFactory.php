<?php

use App\Models\Order;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    $user = factory(User::class)->create();
    $order = [
        "user_id" => $user->id,
        "total_price" => $faker->randomDigit,
        "status" => $faker->randomElement([1,2,3]),
    ];

    return $order;

});


