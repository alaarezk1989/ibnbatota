<?php

use App\Models\Location;
use Faker\Generator as Faker;
use Faker\Factory;

$factory->define( Location::class, function (Faker $faker) {

    $locations = [
        'active' => $faker->boolean,
        'route_id' => 1,
        'code_id' => "m21",
    ];
$arabicFaker = Factory::create('ar_SA');

    foreach (Config::get('app.locales') as $lang => $language) {
        $tempFaker = $lang == 'ar' ? $arabicFaker : $faker;
        $locations[$lang] = [
            "name" => $tempFaker->text(20),
           ];
    }

    return $locations;
});


