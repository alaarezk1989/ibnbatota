<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the 'web' middleware group. Now create something great!
  |
 */


Route::namespace('Web')->group(function () {
    Route::get('/', function () {
        return redirect(route('web.auth.login'));
    });
    Route::post('/attempt', ['uses' => 'AuthController@attempt', 'as' => 'web.auth.attempt']);
    Route::get('/logout', ['uses' => 'AuthController@logout', 'as' => 'web.auth.logout']);
    Route::get('/login', ['uses' => 'AuthController@login', 'as' => 'web.auth.login']);
    Route::get('/register', ['uses' => 'AuthController@register', 'as' => 'web.auth.register']);
    Route::post('/register', ['uses' => 'AuthController@registerAction', 'as' => 'web.auth.register']);
    Route::get('/password/reset', ['uses' => 'AuthController@reset', 'as' => 'password.reset']);
    Route::post('/password/reset', ['uses' => 'AuthController@sendReset', 'as' => 'web.auth.reset.send']);
    Route::post('/password/reset/confirm', ['uses' => 'AuthController@resetPassword', 'as' => 'web.auth.reset.confirm']);
});

Route::namespace('Web')->middleware('auth:web')->group(function () {
    Route::get('/user/profile', ['uses' => 'UserController@edit', 'as' => 'profile.edit']);
    Route::PUT('/user/profile', ['uses' => 'UserController@update', 'as' => 'profile.update']);
    Route::get('/user/password', ['uses' => 'UserController@editPassword', 'as' => 'password.edit']);
    Route::PUT('/user/password', ['uses' => 'UserController@updatePassword', 'as' => 'password.update']);
    // Route::get('/user/tickets', ['uses' => 'TicketsController@myTickets', 'as' => 'web.user.tickets']);
});

Route::prefix('admin')->attribute('namespace', 'Admin')->middleware('admin:web')->group(function () {
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'admin.home.index']);

    Route::prefix('order/{order}')->group(function () {
        Route::resource('products', 'OrderProductsController', ['as' => "admin.order"])
            ->parameter('products', 'orderProduct');
    });
    Route::get('/orders/export', ['uses' => 'OrdersController@export', 'as' => 'admin.orders.export']);
    Route::resource('orders', 'OrdersController', ['as' => 'admin']);
    Route::get('/products/export', ['uses' => 'ProductController@export', 'as' => 'admin.products.export']);
    Route::resource('products', 'ProductController', ['as' => 'admin']);
    Route::delete("product/{product}/image/{productImage}", "ProductController@deleteImage")->name("admin.product.image.destroy");
    Route::resource('routes', 'RoutesController', ['as' => 'admin']);
    Route::get('/users/export', ['uses' => 'UserController@export', 'as' => 'admin.users.export']);
    Route::resource('users', 'UserController', ['as' => 'admin']);
   
    Route::prefix('/user/{user}')->group(function () {
        Route::resource('devices', 'UserDevicesController', ['as' => "admin.user"])
            ->parameter('devices', 'userDevice');
    });

    Route::resource('locations', 'LocationsController', ['as' => 'admin']);
    route::get('/getRouteLocations', 'LocationsController@getRouteLocations')->name('getRouteLocations');
    Route::resource('categories', 'CategoriesController', ['as' => 'admin']);
    Route::resource('/groups', 'GroupController', ['as' => 'admin']);
    Route::resource('/permissions', 'PermissionController', ['as' => 'admin']);
    Route::prefix("/users/{user}")->group(function () {
        Route::resource("groups", "UserGroupsController", ['as' => "admin.users"]);
    });
    Route::resource('/groups', 'GroupController', ['as' => 'admin'])->parameter('group', 'group');
    Route::resource('/permissions', 'PermissionController', ['as' => 'admin']);

    Route::prefix('/group/{group}')->group(function () {
        Route::resource('permissions', 'GroupPermissionController', ['as' => "admin.group"])
            ->parameter('permissions', 'groupPermission');
    });

    // Route::resource('settings', 'SettingsController', ['as' => 'admin'])->only([
    //     "index", "edit", "update"
    // ]);
    Route::get('/logs', ['uses' => 'LogsController@index', 'as' => 'admin.logs.index']);
});


