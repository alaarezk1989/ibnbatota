<?php

use Illuminate\Http\Request;
//auth()->loginUsingId(1);
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->attribute('namespace', 'Api')->group(function () {
    Route::get('/routes', ['uses' => 'RoutesController@index', 'as' => 'api.routes.index']);
    Route::get('/routes/{route}', ['uses' => 'RoutesController@show', 'as' => 'api.routes.index']);
    Route::get('/routes/{route}/locations', ['uses' => 'RoutesController@locations', 'as' => 'api.routes.locations']);
    
    Route::get('/locations', ['uses' => 'LocationsController@index', 'as' => 'api.locations.index']);
    Route::get('/locations/{location}', ['uses' => 'LocationsController@show', 'as' => 'api.locations.show']);
    
    Route::get('/categories', ['uses' => 'CategoriesController@index', 'as' => 'api.categories.index']);
    Route::get('/categories/{category}', ['uses' => 'CategoriesController@show', 'as' => 'api.categories.show']);
    Route::get('/categories/{category}/products', ['uses' => 'CategoriesController@products', 'as' => 'api.categories.products']);
    
    Route::get('/products', ['uses' => 'ProductsController@index', 'as' => 'api.products.index']);
    Route::get('/orders', ['uses' => 'OrdersController@index', 'as' => 'api.products.index']);

    Route::get('/settings', ['uses' => 'SettingsController@index', 'as' => 'api.settings.index']);

    Route::get('/user/types', ['uses' => 'UsersController@types', 'as' => 'api.users.types']);
    Route::get('/order/statuses', ['uses' => 'OrdersController@statuses', 'as' => 'api.order.statuses']);
    Route::post('/addToCard', ['uses' => 'OrdersController@addToCard', 'as' => 'api.auth.addToCard']);
    
});

// Non-Logged in users API's
Route::prefix('v1')->attribute('namespace', 'Api')->group(function () {
    Route::post('/register', ['uses' => 'AuthController@register', 'as' => 'api.auth.register']);
    Route::post('/login', ['uses' => 'AuthController@login', 'as' => 'api.auth.login']);
    Route::post('/logout', ['uses' => 'AuthController@logout', 'as' => 'api.auth.logout']);
    Route::post('/refresh', ['uses' => 'AuthController@refresh', 'as' => 'api.auth.refresh']);
    Route::post('/me', ['uses' => 'AuthController@me', 'as' => 'api.auth.me']);
    Route::post('/password/reset', 'AuthController@resetPassword');
    Route::post('/logout', ['uses' => 'AuthController@logout', 'as' => 'api.auth.logout']);

});


