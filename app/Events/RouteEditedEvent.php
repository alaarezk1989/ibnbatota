<?php

namespace App\Events;


use App\Models\Route;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class RouteEditedEvent
{
    use Dispatchable , SerializesModels;
    public $route;
    public $message;
    public $objectType;
    public $objectId;

    /**
     * routeEditedEvent constructor.
     * @param route $route
     */
    public function __construct(route $route)
    {
        $this->route = $route;
        $this->message = 'updated';
        if ($route->wasChanged('active')) {
            $status = $route->active ? 'active' : 'disabled';
            $this->message = 'changed_status_to_' . $status;
        }

        $this->objectType = get_class($route);
        $this->objectId = $route->id;
    }
}
