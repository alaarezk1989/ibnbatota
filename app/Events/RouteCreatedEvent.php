<?php

namespace App\Events;


use App\Models\Route;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class RouteCreatedEvent
{
    use Dispatchable, SerializesModels;

    public $route;
    public $message;
    public $objectType;
    public $objectId;

    /**
     * routeCreatedEvent constructor.
     * @param route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
        $this->message = 'created';
        $this->objectType = get_class($route);
        $this->objectId = $route->id;
    }
}
