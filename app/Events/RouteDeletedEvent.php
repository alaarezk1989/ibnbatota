<?php

namespace App\Events;


use App\Models\Route;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class RouteDeletedEvent
{
    use Dispatchable, SerializesModels;

    public $route;
    public $message;
    public $objectType;
    public $objectId;

    /**
     * routeDeletedEvent constructor.
     * @param route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
        $this->message = 'deleted';
        $this->objectType = get_class($route);
        $this->objectId = $route->id;
    }
}
