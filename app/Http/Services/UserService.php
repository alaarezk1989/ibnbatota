<?php

namespace App\Http\Services;

use App\Constants\UserTypes;
use App\Models\Branch;
use App\Models\BranchProduct;
use App\Models\Permissible;
use App\Models\User;
use App\Models\Address;
use App\Models\Point;
use App\Models\Contact;
use App\Models\UserDevice;
use App\Repositories\UserRepository;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Hash;
use App\Http\Services\ExportService;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Response;

class UserService
{

    /**
     * @var UploaderService
     */
    private $uploaderService;
    protected $userRepository;

    /**
     * UserService constructor.
     * @param UploaderService $uploaderService
     */
    public function __construct(UploaderService $uploaderService, UserRepository $userRepository)
    {
        $this->uploaderService = $uploaderService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @param null $user
     * @return User|null
     */
    public function fillFromRequest(Request $request, $user = null, $branchesPermission = null)
    {
        if (!$user) {
            $user = new User();
        }
        if ($request->get('password') == '') {
            $request->request->remove('password');
        }
        $user->fill($request->all());
        $user->status = $request->input("status", 0);
        $user->save();
        return $user;
    }


    public function updateProfile(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            throw new Exception('Exception message', Response::HTTP_UNAUTHORIZED);
        }

        if ($request->filled('password') && !Hash::check($request->input('password'), $user->password)) {
            throw new Exception(trans('please_enter_correct_password'), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->fill($request->all());
        if ($request->hasFile('image')) {
            $this->uploaderService->deleteFile($user->image);
            $user->image = $this->uploaderService->upload($request->file('image'), 'users');
        }

        $user->save();

        return true;
    }

    /**
     * @param Request $request
     * @param null $user
     * @return bool
     */
    public function fillChangePasswordFromRequest(Request $request, $user = null)
    {
        if (!$user) {
            return false;
        }
        $currentPassword = $request->input('current_password');
        if (!Hash::check($currentPassword, $user->password)) {
            return false;
        }
        $user->fill($request->all());
        $user->save();
        return true;
    }

    /**
     * @param Request $request
     * @param null $user
     * @return bool|null
     */
    public function fillUserGroupsFromRequest(Request $request, $user = null)
    {
        if (!$user) {
            return false;
        }
        $user->groups()->sync($request->input("groups"));
        return $user;
    }

    /**
     * @param Request $request
     * @param null $address
     *
     * @return Address|null
     */

    public function fillUserDeviceFromRequest(Request $request, $userDevice = null)
    {
        if (!$userDevice) {
            $userDevice = new UserDevice();
        }

        $userDevice->fill($request->request->all());
        $userDevice->save();

        return $userDevice;
    }

    public function export()
    {
        $headings = [
            [trans('users_list')],
            [
                '#',
                trans('name'),
                trans('email'),
                trans('phone')
            ]
        ];
        $list = $this->userRepository->search(request())->get(['id','name', 'email','phone']);

        return Excel::download(new ExportService($list, $headings), 'Users Report.xlsx');
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function resetPhoneVerified()
    {
        $user = auth()->user();
        $user->phone_verify = code($numbers = 4, $type = 'digits');
        $user->save();
        return $user;
    }

    public function verifyUserPhone(Request $request)
    {
        $user = auth()->user();
        if ($user->phone_verify != $request->get("code")) {
            throw new Exception('Invalid Code', Response::HTTP_NOT_ACCEPTABLE);
        }
        $user->phone_verify = '';
        $user->active = true;
        $user->save();

        return true;
    }

    public function toggleNotification(Request $request)
    {
        $user = auth()->user();
        $user->notification = $request->get('notification', 0);
        $user->save();

        return $user;
    }

    public function uploadProfilePicture(Request $request)
    {
        $user = auth()->user();

        if ($request->hasFile('image')) {
            $this->uploaderService->deleteFile($user->image);
            $user->image = $this->uploaderService->upload($request->file('image'), 'users');

            $user->save();
            return $user->image;
        }
        return false;
    }
}
