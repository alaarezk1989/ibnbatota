<?php

namespace App\Http\Services;

use App\Constants\OrderStatus;
use App\Constants\OrderTypes;
use App\Models\Location;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Repositories\OrderRepository;
use App\Http\Services\ExportService;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\Orders;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\Request;
use App\Constants\UserTypes ;

class OrderService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function fillFromRequest(Request $request, $order = null)
    {
        if (!$order) {
            $order = new order();
        }

        $order->fill($request->request->all());
        $order->status = OrderStatus::SUBMITTED;
        $order->save();
        return $order;
    }

    public function fillApiRequest(Request $request, $order = null)
    {
        if (!$order) {
            $order = new order();
        }
        $user = User::wherePhone($request->phone)->first() ;
        if(!$user){
            $user = User::create($request->all());
        }
        $request = array_merge($request->all(), ['user_id' => $user->id ]);

        $order->fill($request);

        $order->save();
    
        $order->total_price = $this->fillProducts($order , $request['products']);
        $order->save();

        return $order;
    }

    
    public function fillProducts($order, $request)
    {
        $totalPrice = 0 ;
        foreach($request as $product){
            $original = Product::find($product['product_id']);
            
            $orderProduct = OrderProduct::create(['order_id' => $order->id, 'product_id' => $product['product_id'],'price' => $original->price ,
                'quantity' => $product['quantity'], 'total' => ( $product['quantity'] * $original->price ) ]);
        
            $totalPrice += $orderProduct->total ;
        }

        return $totalPrice ;
    }
    public function export()
    {
        $headings = [
            [trans('orders_list')],
            ['#', 
                trans('user_name'),
                trans('user_phone'),
                trans('total_price'),
                trans('created_at'),
                trans('status')
            ]
        ];
        $list = $this->orderRepository->search(request())->get();
        $listObjects = Orders::collection($list);

        return Excel::download(new ExportService($listObjects, $headings), 'Orders Report.xlsx');
    }
}