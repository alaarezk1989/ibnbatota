<?php

namespace App\Http\Services;

use App\Models\Product;
use App\Models\ProductImage;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\Products;
use File;

class ProductService
{

    private $uploaderService;
    protected $productRepository;

    public function __construct(UploaderService $uploaderService, ProductRepository $productRepository)
    {
        $this->uploaderService = $uploaderService;
        $this->productRepository = $productRepository;
    }

    public function fillFromRequest(Request $request, $product = null)
    {
        if (!$product) {
            $product = new Product() ;
        }
        $product->fill($request->all());
        $product->active = $request->input("active", 0);
 
        if ($request->has("image")) {
            $image= $request->file('image'); 
            $image = $this->uploaderService->upload($image, "products");
            $product->image = $image ;
        }
        
        $product->save() ;
        return $product ;
    }

   
    public function export()
    {
        $headings = [
            [trans('products_list')],
            [
                '#',
                trans('category'),
                trans('name'),
                trans('description'),
                trans('status')
            ]
        ];

        $list = $this->productRepository->search(request())->get();
        $listObjects = Products::collection($list);

        return Excel::download(new ExportService($listObjects, $headings), 'Products Report.xlsx');
    }
}
