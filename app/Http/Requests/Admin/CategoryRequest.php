<?php

namespace App\Http\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        foreach (config()->get("app.locales") as $key => $lang) {
            $rules[$key.".*"] = "required" ;
        }

        if ($this->method() == 'POST') {
            $rules["image"] = "image|mimes:jpeg,png,jpg,gif,svg|max:50|dimensions:ratio=100/100";
        }
        if ($this->method() == 'PUT') {
            $rules['parent_id'] = 'not_in:'.$this->id;
            if ($this->file('image')) {
                $rules["image"] = "image|mimes:jpeg,png,jpg,gif,svg|max:50|dimensions:ratio=100/100";
            }
        }

        return $rules;
    }
}
