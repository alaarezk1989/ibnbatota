<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use \App\Constants\UserTypes as UserTypes;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $passwordRules = 'min:8|max:100|confirmed';

        $rules = [
            'name'        => 'required|min:2|max:45',
            'email'       => 'nullable|email|unique:users,email,NULL,id,deleted_at,NULL',
            'phone'       => 'required|regex:/(0)[0-9\s-]{10}/|unique:users,phone,NULL,id,deleted_at,NULL',
            'type'        => 'required',
            'route_id'        => 'required',
            'location_id'        => 'required'
        ];

        if ($this->method() == 'POST') {
            $rules['password'] = $passwordRules;
        }

        if ($this->method() == 'PUT') {
            $rules['name'] = $rules['name'].",name";
            $rules['email'] = $rules['email'].",email,".$this->id;
            $rules['phone'] = $rules['phone'].",phone,".$this->id;
            $rules['type'] = $rules['type'];
            if ($this->password) {
                $rules['password'] = $passwordRules;
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => trans('name_required'),
            'location_id.required' => trans('location_id_required'),
            'route_id.required' => trans('route_id_required'),
            'name.min'      => trans('name_shoud_be_at_least_2'),
            'name.max'      => trans('name_shoud_not_be_more_than_45'),
            'email.required'       => trans('email_required'),
            'email.unique'       => trans('email_already_taken'),
            'phone.required'       => trans('phone_required'),
            'phone.regex'       => trans('phone_should_be_in_right_format_like_01--_and_shoud_be_11_numbers'),
            'type.required'       => trans('user_type_required'),
            'password.required' => trans('password_required'),
            'password.min' => trans('password_shoud_be_at_least_8'),
            'password.max' => trans('password_shoud_not_be_more_than_45'),
            'password.confirmed' => trans('two_password_should_be_same'),
        ];
    }
}
