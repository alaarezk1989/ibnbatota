<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'name' => 'required',
            'products' => 'required|array',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'=>trans('user_id_required'),
            'address_id.required'=>trans('address_id_required'),
        ];
    }

}
