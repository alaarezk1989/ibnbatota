<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone'            => 'required|regex:/(0)[0-9\s-]{10}/',
            'password'         => 'required|min:8',
        ];
    }

    public function messages()
    {
        return [
            'phone.required'       => trans('phone_required'),
            'password.required'         => trans('password_required'),
            'password.min'              => trans('password_min'),
            'confirm_password.required' => trans('confirm_password_required'),
            'confirm_password.same'     => trans('confirm_password_same'),
        ];
    }

}
