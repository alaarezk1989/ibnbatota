<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'       => 'required|min:4|max:45',
            'email'            => 'nullable|email|unique:users',
            'phone'            => 'required|unique:users|regex:/(0)[0-9\s-]{10}/',
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'phone.required'       => trans('phone_required'),
            'name.required'       => trans('name_required'),
            'email.email'               => trans('email_email_validation'),
            'password.required'         => trans('password_required'),
            'password.min'              => trans('password_min'),
            'confirm_password.required' => trans('confirm_password_required'),
            'confirm_password.same'     => trans('confirm_password_same'),
        ];
    }

}
