<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LocationResource extends Resource
{
    public function toArray($request)
    {

        return [
                'id' => $this->id,
                'name' => $this->name ,
                'code_id' => $this->code_id
        ];
    }
}
