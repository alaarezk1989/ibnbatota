<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryResource extends Resource
{
    public function toArray($request)
    {

        return [
                'id' => $this->id,
                'name' => $this->name,
                'status' => $this->active
        ];
    }
}
