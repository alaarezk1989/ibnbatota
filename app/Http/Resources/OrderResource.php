<?php

namespace App\Http\Resources;

use App\Constants\OrderStatus;
use Illuminate\Http\Resources\Json\Resource;

class OrderResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_phone' => $this->user? $this->user->phone: '',
            'user_name' => $this->user? $this->user->name: '',
            'price' => $this->total_price,
            'status' => OrderStatus::getValue($this->status),
            'created_at' => $this->created_at,
        ];
    }
}
