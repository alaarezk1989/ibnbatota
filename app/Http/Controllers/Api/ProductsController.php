<?php

namespace App\Http\Controllers\Api;

use App\Models\BranchProduct;
use App\Repositories\ProductRepository;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Resources\ProductResource ;

class ProductsController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(Request $request)
    {
        $list = $this->productRepository->searchFromRequest($request)->paginate(20);

        return ProductResource::collection($list)
        ->additional(['status'=>200, 'message'=> 'returned successfully']);

    }

}
