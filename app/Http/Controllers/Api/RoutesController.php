<?php

namespace App\Http\Controllers\Api;

use App\Models\Route;
use App\Repositories\RouteRepository;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Resources\RouteResource ;
use App\Http\Resources\LocationResource ;

class RoutesController extends Controller
{
    protected $routeRepository;
    public function __construct(RouteRepository $routeRepository)
    {
        $this->routeRepository = $routeRepository;
    }

    public function index(Request $request)
    {
        $list = $this->routeRepository->search($request)->paginate(10);
        return RouteResource::collection($list)
        ->additional(['status'=>200, 'message'=> 'returned successfully']);
    }

    public function show(Route $route)
    {
        return response()->json($route);
    }

    public function locations(Route $route)
    {
        return  LocationResource::collection($route->locations()->paginate(20))
        ->additional(['status'=>200, 'message'=> 'returned successfully']);;
    }
}
