<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Resources\CategoryResource ;
use App\Http\Resources\ProductResource; 
class CategoriesController extends Controller
{
    protected $categoryRepository;
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $list = $this->categoryRepository->searchFromRequest($request)->paginate(20);

        return CategoryResource::collection($list)
        ->additional(['status'=>200, 'message'=> 'returned successfully']);

    }

    public function show(Category $category)
    {
        return response()->json($category);
    }
    
    function products(Category $category)
    {
        return ProductResource::collection($category->products()->paginate(20))
            ->additional(['status'=>200, 'message'=> 'returned successfully']);

    }

}
