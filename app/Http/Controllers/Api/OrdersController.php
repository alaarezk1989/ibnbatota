<?php

namespace App\Http\Controllers\Api;

use App\Constants\OrderStatus;
use App\Http\Requests\Api\ValidatePromotionCodeRequest;
use App\Models\Order;
use Illuminate\Routing\Controller;
use App\Http\Services\OrderService;
use App\Http\Services\NotificationService;
use App\Http\Requests\Api\OrderRequest;
use Illuminate\Http\Request;
use App\Repositories\OrderRepository;
use App\Http\Resources\OrderResource ;

class OrdersController extends Controller
{
    protected $orderService;
    protected $orderRepository;

    public function __construct(OrderService $orderService , OrderRepository $orderRepository)
    {
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $list = $this->orderRepository->search($request)->paginate(20);
        return OrderResource::collection($list)
        ->additional(['status'=>200, 'message'=> 'returned successfully']);

    }

    public function addToCard(OrderRequest $request, OrderService $orderService)
    {
        $order = $orderService->fillApiRequest($request);
        return response()->json(['order' => $order, 'status' =>200, 'message'=>"order created successfully" ]);
    }


    public function show(Order $order)
    {
        $order = $order->with(['products','products.product','address','branch.zones'])
                ->find($order->id);

        return response()->json($order);
    }

    
}
