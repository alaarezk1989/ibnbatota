<?php

namespace App\Http\Controllers\Api;

use App\Http\Services\NotificationService;
use Illuminate\Routing\Controller;
use App\Http\Services\AuthService;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\UserRequest;
use App\Http\Requests\Web\ForgotPasswordRequest;
use Password;
use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\HttpFoundation\Request;
use Illuminate\Http\Request;
use App\Models\User ;
use Lcobucci\JWT\Parser;
use App\Http\Requests\Api\UpdatePasswordRequest ;

use Auth ;

class AuthController extends Controller
{
    public $authService ;

    public function __Construct(AuthService $authService)
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
        $this->authService = $authService ;
    }

    public function register(RegisterRequest $request)
    {    
        $user = User::create($request->all());
        $user->token = auth('api')->login($user);
        return response()->json([
            'message' => 'User Created ',
            'data' => ['user' => $user],
            'status' => 200
        ]);
    }

    public function login(LoginRequest $loginRequest)
    {
        $user = User::wherePhone($loginRequest->get('phone'))->whereType(1)->first();
        
        if (!$user) {
            return response()->json([
                'message' => "unauthorized",
                "data" => [],
                'code' => 401
            ], 401);
        }       
     

        $user->token =  auth('api')->login($user);
        return response()->json([
            'message' => 'logged in successfully',
            'data' => [
                'user' => $user
            ],
            'code' => 200

        ]);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function update(UserRequest $request)
    {
        User::where('id', auth()->user()->id)->update($request->all());

        return response(['success' => true], Response::HTTP_CREATED);
    }

    public function resetPassword(ForgotPasswordRequest $request)
    {
        Password::broker()->sendResetLink(
            $request->only('email')
        );

        return response(['success' => true], Response::HTTP_CREATED);
    }

    public function passwordUpdate(UpdatePasswordRequest $request)
    {
        return $this->authService->passwordUpdate($request) ;
    }
}
