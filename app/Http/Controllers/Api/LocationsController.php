<?php

namespace App\Http\Controllers\Api;

use App\Models\Location;
use App\Repositories\LocationRepository;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Resources\LocationResource ;
use App\Http\Resources\LocationCollection ;
class LocationsController extends Controller
{
    protected $locationRepository;
    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function index(Request $request)
    {
        $list = $this->locationRepository->searchFromRequest($request)->paginate(20);
        return  LocationResource::collection($list)
            ->additional(['status'=>200, 'message'=> 'returned successfully']);
    }

    public function show(Location $location)
    {
        return response()->json($location);
    }

}
