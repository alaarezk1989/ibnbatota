<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RouteRequest;
use App\Http\Services\RouteService;
use App\Models\Route;
use App\Repositories\RouteRepository;
use View;

class RoutesController extends Controller
{

    protected $RouteService;
    private $RouteRepository;

    public function __construct(RouteService $routeService, RouteRepository $routeRepository)
    {
        $this->authorizeResource(Route::class, "route");
        $this->routeService = $routeService;
        $this->routeRepository = $routeRepository;
    }

    public function index()
    {
        $this->authorize("index", Route::class);

        $list = $this->routeRepository->search(request())->paginate(10);

        $list->appends(request()->all());

        return View::make('admin.routes.index', ['list' => $list]);
    }

    public function create()
    {
        return View::make('admin.routes.new');
    }

    public function store(RouteRequest $request)
    {
        $this->routeService->fillFromRequest($request);
        return redirect(route('admin.routes.index'))->with('success', trans('item_added_successfully'));
    }

    public function destroy(Route $route)
    {
        $route->delete();

        return redirect()->back()->with('success', trans('item_deleted_successfully'));
    }

    public function edit(Route $route)
    {
        return View::make('admin.routes.edit', ['route' => $route]);
    }

    public function update(RouteRequest $request, Route $route)
    {
        $this->routeService->fillFromRequest($request, $route);

        return redirect(route('admin.routes.index'))->with('success', trans('item_updated_successfully'));
    }

}
