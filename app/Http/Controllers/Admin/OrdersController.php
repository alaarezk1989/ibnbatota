<?php

namespace App\Http\Controllers\Admin;

use App\Constants\OrderStatus;
use App\Constants\OrderTypes;
use App\Constants\UserTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrderRequest;
use App\Http\Services\OrderService;
use App\Models\Order;
use App\Models\User;
use App\Models\Address;
use App\Repositories\OrderRepository;
use View;

class OrdersController extends Controller
{
    protected $orderService;
    protected $orderRepository;

    public function __construct(OrderService $orderService, OrderRepository $orderRepository)
    {
        $this->authorizeResource(Order::class, 'order');
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
    }
   
    public function index()
    {
        $this->authorize("index", Order::class);
        $status = OrderStatus::getList();
        $types= OrderTypes::getList();
        $ordersCount = $this->orderRepository->search(request())->count();
        
        $list = $this->orderRepository->search(request())->paginate(10);

        $list->appends(request()->all());
        return View::make('admin.orders.index', ['list' => $list, 'status' => $status, 'ordersCount' => $ordersCount, 'types' => $types]);
    }

    // public function create()
    // {
    //     $users = User::where('type', UserTypes::NORMAL)->where('active', 1)->orderBy('id', 'DESC')->get();

    //     return View::make('admin.orders.new', ['users'=>$users]);
    // }

    // public function store(OrderRequest $request)
    // {
    //     $this->orderService->fillFromRequest($request);

    //     return redirect(route('admin.orders.index'))->with('success', trans('order_added_successfully'));
    // }

    // public function edit(Order $order)
    // {
    //     $users = User::where('type', UserTypes::NORMAL)->where('active', 1)->orderBy('id', 'DESC')->get();
    //     return View::make('admin.orders.edit', ['order' => $order,'users'=>$users]);
    // }

    // public function update(OrderRequest $request, Order $order)
    // {
    //     $this->orderService->fillFromRequest($request, $order);

    //     return redirect(route('admin.orders.index'))->with('success', trans('order_updated_successfully'));
    // }

    public function show(Order $order)
    {
        $products = $order->products;
        return View::make('admin.orders.show', ['order' => $order, 'products' => $products]);
    }

    public function destroy(Order $order)
    {        
        $order->products()->detach();
        $order->delete();
        return redirect()->back()->with('success', trans('item_deleted_successfully'));
    }

    public function export()
    {
        return $this->orderService->export();
    }
}
