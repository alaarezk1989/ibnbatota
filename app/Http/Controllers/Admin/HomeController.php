<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\OrderRepository;

use \App\Constants\OrderStatus;
use App\Constants\UserTypes;
use App\Repositories\UserRepository;
use View;

class HomeController extends Controller
{
    protected $orderRepository;
    protected $userRepository;

    public function __construct( OrderRepository $orderRepository, UserRepository $userRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $user = auth()->user();
        $orders = $this->ordersCount();
        $users = $this->userCounts();

        return View::make('admin.home.index', ['orders' => $orders, 'users' => $users]);
    }

 
    public function ordersCount()
    {
        $orders ['totalOrders'] = $this->orderRepository->search(request())->count();

        request()->query->set('status', OrderStatus::SUBMITTED);
        $orders ['submitted'] = $this->orderRepository->search(request())->count();

        request()->query->set('status', OrderStatus::CANCELLED);
        $orders ['cancelled'] = $this->orderRepository->search(request())->count();

        request()->query->set('status', OrderStatus::DELIVERED);
        $orders ['delivered'] = $this->orderRepository->search(request())->count();

        return $orders;
    }

    public function userCounts()
    {
        request()->query->set('type', UserTypes::NORMAL);
        $users['customers'] = $this->userRepository->search(request())->count();

        
        return $users;
    }
}
