<?php

namespace App\Http\Controllers\Admin;

use App\Constants\UserTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Services\UploaderService;
use App\Http\Services\UserService;
use App\Models\User;
use App\Models\Location;
use App\Models\Route;
use App\Repositories\UserRepository;
use View;

class UserController extends Controller
{
    protected $userService;
    private $userRepository;
    private $uploaderService;

    public function __construct(UserService $userService, UserRepository $userRepository, UploaderService $uploaderService)
    {
        $this->authorizeResource(User::class, "user");
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->uploaderService = $uploaderService;
    }

    public function index()
    {
        $this->authorize("index", User::class);
        $list = $this->userRepository->search(request())->paginate(10);
        $list->appends(request()->all());
        $count = $this->userRepository->search(request())->count();
        $types = UserTypes::getList();

        return View::make('admin.users.index', ['list' => $list, 'count' => $count, 'types' => $types,]);
    }

    public function create()
    {
        $locations = Location::where('active', 1)->get();
        $routes = Route::where('active', 1)->get();

        return View::make('admin.users.create', ['locations' => $locations, 'routes' => $routes]);
    }

    public function store(UserRequest $request)
    {
        $user = $this->userService->fillFromRequest($request);
        return redirect(route('admin.users.index'))->with('success', trans('item_added_successfully'));
    }

    public function edit(User $user)
    {
        $locations = Location::where('active', 1)->get();
        $routes = Route::where('active', 1)->get();

        return View::make('admin.users.edit', ['user' => $user, 'locations' => $locations, 'routes' => $routes]);
    }

    public function update(UserRequest $request, User $user)
    {
        $this->userService->fillFromRequest($request, $user);
        return redirect(route('admin.users.index'))->with('success', trans('item_updated_successfully'));
    }

    public function destroy(User $user)
    {
        if (in_array($user->id, [1, 2])) {
            return redirect()->back()->with('danger', trans('cant_delete_this_user'));
        }
        $this->uploaderService->deleteFile($user->image);
        $user->delete();

        return redirect()->back()->with('success', 'Item Deleted Successfully');
    }

    public function export()
    {
        return $this->userService->export();
    }
}
