<?php

namespace App\Http\Controllers\Admin;

use App\Constants\BranchTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Services\ProductService;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Unit;
use App\Models\Category;
use App\Repositories\ProductRepository;
use View;

class ProductController extends Controller
{

    /**
     * @var ProductService
     */
    private $productService;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductController constructor.
     * @param ProductService $productService
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductService $productService, ProductRepository $productRepository)
    {
        $this->productService = $productService;
        $this->productRepository = $productRepository;
        $this->authorizeResource(Product::class, "product");
    }

    public function index()
    {
        $this->authorize("index", Product::class);
        $list = $this->productRepository->search(request())->paginate(10);
        $list->appends(request()->all());
        $count = $this->productRepository->search(request())->count();
        return View::make("admin.products.index", compact("list", "count"));
    }

    public function create()
    {
        $categories = Category::all();

        return View::make("admin.products.create", compact( "categories"));
    }

    public function store(ProductRequest $request)
    {
        $product = $this->productService->fillFromRequest($request);
        return redirect(route("admin.products.index"))->with('success', trans('item_added_successfully'));
    }

    public function edit(Product $product)
    {
        $categories = Category::all();
        return View::make("admin.products.edit", compact("product", 'categories'));
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product = $this->productService->fillFromRequest($request, $product);
        return redirect(route("admin.products.index"))->with('success', trans('item_updated_successfully'));
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return back()->with('success', trans('item_deleted_successfully'));
    }

    public function export()
    {
        return $this->productService->export();
    }
}
