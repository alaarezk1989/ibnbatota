<?php

namespace App\Listeners;

use App\Events\RouteDeletedEvent;
use App\Events\RouteEditedEvent;
use App\Events\RouteCreatedEvent;
use App\Http\Services\LogsService;

class RouteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $logsService;

    /**
     * RouteListener constructor.
     * @param LogsService $logsService
     */
    public function __construct(LogsService $logsService)
    {
        $this->logsService = $logsService;
    }

    /**
     * Handle the event.
     *
     * @param RouteEditedEvent $event
     * @return void
     */
    public function handleEditedRoute(RouteEditedEvent $event)
    {
        $this->logsService->fillLog($event->objectId, $event->objectType, $event->message);
    }

    /**
     * @param RouteDeletedEvent $event
     */
    public function handleDeletedRoute(RouteDeletedEvent $event)
    {
        $this->logsService->fillLog($event->objectId, $event->objectType, $event->message);
    }

    /**
     * @param RouteCreatedEvent $event
     */
    public function handleCreatedRoute(RouteCreatedEvent $event)
    {
        $this->logsService->fillLog($event->objectId, $event->objectType, $event->message);
    }
}
