<?php

namespace App\Policies;

use App\Constants\UserTypes;
use App\Models\User;
use App\Models\Route;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoutePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the Route index.
     *
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        // check if the user is not admin return false before access any rolls
        if (!$user->isTypeOf(UserTypes::ADMIN)) {
            return false;
        }
    }

    /**
     * Determine whether the user can view the Route index.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return $user->hasAccess("admin.routes.index");
    }

    /**
     * Determine whether the user can create Routes.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess("admin.routes.create");
    }

    /**
     * Determine whether the user can update the Route.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Route  $route
     * @return mixed
     */
    public function update(User $user, Route $route)
    {
        return $user->hasAccess("admin.routes.update");
    }

    /**
     * Determine whether the user can delete the Route.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Route  $route
     * @return mixed
     */
    public function delete(User $user, Route $route)
    {
        return $user->hasAccess("admin.routes.destroy");
    }
}
