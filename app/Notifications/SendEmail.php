<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Notification
{

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $templateId;
    protected $payload  ;

    public function __construct($templateId, $payload = [])
    {
        $this->templateId = $templateId;
        $this->payload = $payload ;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

/**
 * $user = User::find(1);
 *   $user->notify(new SendMail());
 *
 */
