<?php

namespace App\Constants;

use phpDocumentor\Reflection\Types\Self_;

final class ObjectTypes
{
    const PRODUCT = 'App\Models\Product';
    const ORDER = 'App\Models\Order';
    const CATEGORY = 'App\Models\Category';
    const USER = 'App\Models\User';
    const LOCATION = 'App\Models\Location';
    const GROUP = 'App\Models\Group';
    const GROUPPERMISSION = 'App\Models\GroupPermission';
    
    public static function getKeyList()
    {
        return array_keys(self::getList());
    }

    public static function getList()
    {
        return [
            ObjectTypes::PRODUCT => trans("products"),
            ObjectTypes::ORDER => trans("orders"),
            ObjectTypes::CATEGORY => trans("categories"),
            ObjectTypes::USER => trans("users"),
            ObjectTypes::LOCATION => trans("locations"),
            ObjectTypes::GROUP => trans("groups"),
            ObjectTypes::GROUPPERMISSION => trans("groupPermissions"),
        ];
    }

    public static function getOne($index = '')
    {
        $list = self::getList();
        $list_one = '';
        if ($index) {
            $list_one = $list[$index];
        }
        return $list_one;
    }
}
