<?php

namespace App\Constants;

final class OrderStatus
{
    const SUBMITTED = 1;
    const ASSIGNED = 2;
    const DELIVERED = 3;
    const CANCELLED = 4;


    public static function getList()
    {
        return [
            OrderStatus::SUBMITTED    => trans("submitted"),
            OrderStatus::ASSIGNED    => trans("assigned"),
            OrderStatus::DELIVERED => trans("delivered"),
            OrderStatus::CANCELLED => trans("cancelled"),
        ];
    }

    public static function getValue($key)
    {
        $list = self::getList();

        return isset($list[$key]) ? $list[$key] : '';
    }
}
