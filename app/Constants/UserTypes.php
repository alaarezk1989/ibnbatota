<?php

namespace App\Constants;

final class UserTypes
{
    const NORMAL = 1;
    const ADMIN  = 2;
    

    public static function getList()
    {
        return [
            UserTypes::NORMAL => trans("client"),
            UserTypes::ADMIN => trans("admin"),
           
        ];
    }

    public static function getTypesUrl()
    {
        return [
            UserTypes::ADMIN => "admin",
            UserTypes::NORMAL => "normal",
        ];
    }
  
    public static function getOne($index = '')
    {
        $list = self::getList();
        $listOne = '';
        if ($index) {
            $listOne = $list[$index];
        }
        return $listOne;
    }
}
