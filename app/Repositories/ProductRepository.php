<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\BranchProduct;
use Symfony\Component\HttpFoundation\Request;

class ProductRepository
{

    public function search(Request $request)
    {
        $products = Product::query()->orderByDesc("id");
        

        if ($request->get('filter_by') == "category_id" && !empty($request->get('q'))) {
            $products->where("category_id", '=', $request->get("q"));
        }
        if ($request->get('filter_by') == "name" && !empty($request->get('q'))) {
            $products->whereHas('translations', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->query->get('q') . '%');
            });
        }
       
        if ($request->has('from_date') && !empty($request->get('from_date'))) {
            $products->where('created_at', '>=', $request->get('from_date'));
        }

        if ($request->has('to_date') && !empty($request->get('to_date'))) {
            $products->where('created_at', '<=', $request->get('to_date'));
        }

        return $products;
    }
    
    public function searchFromRequest($request)
    {
        $products = Product::orderBy('id', 'DESC');

        if ($request->query->has('category')) {
            $products->where('category_id', '=', $request->query->get('category'));
        }

        return $products;
    }

    public function getProductPrice($productId)
    {
        $product = Product::where('id', '=', $productId);
        return $product;
    }

  
}
