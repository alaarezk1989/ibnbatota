<?php

namespace App\Repositories;

use App\Models\Route;
use Symfony\Component\HttpFoundation\Request;

class RouteRepository
{
    public function search(Request $request)
    {
        $routes = Route::query()->orderByDesc("id");

        if ($request->filled('title') ) {
            $routes->whereHas('translations', function ($query) use ($request) {
                $query->where('title', 'like', '%' . $request->query->get('title') . '%');
            });
        }

        return $routes;
    }
}
