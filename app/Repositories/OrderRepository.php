<?php

namespace App\Repositories;

use App\Constants\DriverOrderStatus;
use App\Constants\OrderStatus;
use App\Constants\UserTypes;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Permissible;
use App\Models\User;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;

class OrderRepository
{
    public function search(Request $request)
    {
        $orders = Order::query()->orderByDesc("id");

        if ($request->get('status') && !empty($request->get('status'))) {
            $orders->where('status', $request->query->get('status'));
        }
        if ($request->get('type') && !empty($request->get('type'))) {
            $orders->whereHas('branch', function ($query) use ($request) {
                $query->where('type', $request->query->get('type'));
            });
        }
        if ($request->has('from_date') && !empty($request->get('from_date'))) {
            $orders->where('created_at', '>=', $request->get('from_date'));
        }
        if ($request->has('to_date') && !empty($request->get('to_date'))) {
            $orders->where('created_at', '<=', $request->get('to_date'));
        }
        return $orders;
    }

}