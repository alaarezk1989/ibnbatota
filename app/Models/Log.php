<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array user
 */
class Log extends Model
{

    protected $fillable = ['user_id','object_id','object_type','message'];
    public function User(Type $var = null)
    {
       return $this->belongsTo(User::class);
        
    }
}
