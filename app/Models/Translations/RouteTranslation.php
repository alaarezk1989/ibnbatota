<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;

class RouteTranslation extends Model
{

    protected $table = 'routes_translations';


    protected $fillable = ['title'];
}
