<?php

namespace App\Models;

use App\Constants\UserTypes;
use App\Events\UserCreatedEvent;
use App\Events\UserDeletedEvent;
use App\Events\UserEditedEvent;
use App\Overrides\ResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

use PhpParser\Node\Expr\Array_;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{

    use Notifiable;
    use SoftDeletes;
    use CanResetPassword;

    protected $fillable =[
        'name',
        'password',
        'email',
        'phone',
        'status',
        'type',
        'route_id',
        'location_id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = \Hash::make($pass);
    }

    protected $attributes = [
        'type' => UserTypes::NORMAL
    ];

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'user_groups', 'user_id', 'group_id');
    }


    public function devices()
    {
        return $this->hasMany(UserDevice::class, 'user_id', 'id');
    }

    public $dispatchesEvents = [
        'created' => UserCreatedEvent::class,
        'updated' => UserEditedEvent::class,
        'deleted' => UserDeletedEvent::class,
    ];

    public function hasAccess($permission, $permissible = null): bool
    {
        $userPermissions = $this->permissions();

        if (!$permissible || !count($this->permissionsData(get_class($permissible)))) {
            return in_array($permission, $userPermissions);
        }
        
        if (count($permissible->permissible) && Permissible::where('user_id', auth()->user()->id )->where('permissible_id', $permissible->id)->where('permissible_type', get_class($permissible))->first()) {
            return in_array($permission, $userPermissions);
        }
        return false;
    }

    public function permissions() : array
    {
        $permissions = $this->query()
            ->join("user_groups", "users.id", "=", "user_groups.user_id")
            ->join("groups", "user_groups.group_id", "=", "groups.id")
            ->join("group_permissions", "groups.id", "=", "group_permissions.group_id")
            ->join("permissions", "group_permissions.permission_id", "=", "permissions.id")
            ->select("permissions.identifier")
            ->where("users.id", "=", auth()->id())
            ->distinct()
            ->get();

        $permissionsIdentifier = [];
        foreach ($permissions as $permission) {
            $permissionsIdentifier[] = $permission["identifier"];
        }

        return $permissionsIdentifier;
    }

    public function permissionsData($modelType) : array
    {
        $permissions = $this->query()
            ->join("user_permissible", "users.id", "=", "user_permissible.user_id")
            ->select("user_permissible.permissible_id")
            ->where("users.id", "=", auth()->id())
            ->where("user_permissible.permissible_type", "=", $modelType)
            ->distinct()
            ->get();

        $permissionsIdsArray = [];
        foreach ($permissions as $permission) {
            $permissionsIdsArray[] = $permission["permissible_id"];
        }

        return $permissionsIdsArray;
    }

    public function isTypeOf($userType)
    {
        return ($this->type == $userType);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
