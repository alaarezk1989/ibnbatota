<?php

namespace App\Models;

use App\Events\RouteDeletedEvent;
use App\Events\RouteEditedEvent;
use App\Events\RouteCreatedEvent;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model
{
    use Translatable;
    use SoftDeletes;

    protected $table = 'routes';
    public $translatedAttributes = ['title'];
    protected $appends = ['title'];
    protected $hidden = ['translations'];
    protected $fillable = ['active'];

    public function getTitleAttribute()
    {
        return $this->getTranslationByLocaleKey(app()->getLocale())->title;
    }

    public $dispatchesEvents = [
        'updated' => RouteEditedEvent::class,
        'deleted' => RouteDeletedEvent::class,
        'created' => RouteCreatedEvent::class
    ];

    public function locations()
    {

        return $this->hasMany(Location::class);
    }
}
