<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\LocationDeletedEvent ;
use App\Events\LocationUpdatedEvent ;
use App\Events\LocationCreatedEvent ;

class Location extends Model
{

    use Translatable;
    use SoftDeletes;

    protected $table = 'locations';
    public $translatedAttributes = ['name'];
    protected $appends = ['name'];
    protected $hidden = ['translations'];
    protected $fillable = ['active', 'route_id', 'code_id'];

    
    public function getNameAttribute()
    {
        return $this->getTranslationByLocaleKey(app()->getLocale())->name;
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public $dispatchesEvents = [

        'updated' => LocationUpdatedEvent::class,
        'deleted' => LocationDeletedEvent::class,
        'created' => LocationCreatedEvent::class,
    ];

}
