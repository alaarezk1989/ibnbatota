<?php

namespace App\Models;

use App\Constants\OrderStatus;
use App\Events\OrderCreatedEvent;
use App\Events\OrderEditedEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';

    protected $fillable = [
        'status',
        'total_price',
        'user_id',
    ];
    
    protected $attributes = [
        'status' =>  OrderStatus::ASSIGNED,
        'total_price' =>  0
    ];
    protected $hidden = [
        'updated_at'
    ];
    public function getStatusAttribute($val)
    {
        return OrderStatus::getValue($val);
    }

    public $dispatchesEvents = [
        'updated' => OrderEditedEvent::class,
        'created' => OrderCreatedEvent::class,
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products', 'order_id', 'product_id')
            ->withPivot('id', 'price', 'quantity')
            ->where('order_products.deleted_at', null);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::Class);
    }

}
