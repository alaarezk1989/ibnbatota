<?php

namespace App\Models;

use App\Events\ProductCreatedEvent;
use App\Events\ProductDeletedEvent;
use App\Events\ProductEditedEvent;
use App\Http\Services\UploaderService ;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['name', 'description'];
    protected $appends = ['name', 'description'];
    protected $hidden = ['translations'];

    protected $fillable = [
        "category_id",
        "image",
        "code",
        "price",
        "discount",
        "active",
    ];

    public $dispatchesEvents = [
        'created' => ProductCreatedEvent::class,
        'updated' => ProductEditedEvent::class,
        'deleted' => ProductDeletedEvent::class
    ];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getNameAttribute()
    {
        return $this->getTranslationByLocaleKey(app()->getLocale())->name;
    }

    public function orderProduct()
    {
        return $this->hasMany(OrderProduct::class);
    }


}
