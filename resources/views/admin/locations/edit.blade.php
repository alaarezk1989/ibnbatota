@extends('admin.layout')
@section('content')
<?php
use \App\Constants\ClassTypes;
?>
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{ trans('new_location')." ( ".trans('zones'). " ) "  }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">{{ trans('home') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.locations.index') }}" class="text-light-color">{{ trans('locations') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ trans('update_location') }} #{{ $location->id }}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ trans('update_location')." ( ".trans('zones'). " ) "  }}</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')
                                <form action="{{ route('admin.locations.update', ['location' => $location]) }}" method="post">
                                    @method("PUT")
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $location->id}}" />
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $lang == app()->getLocale() ? 'active': ''}} show" id="home-tab2" data-toggle="tab" href="#lang-{{ $lang }}" role="tab" aria-controls="home" aria-selected="true">{{ $language }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <div class="tab-pane fade {{ $lang == app()->getLocale() ? 'active show': ''}}" id="lang-{{ $lang }}" role="tabpanel" aria-labelledby="home-tab2">
                                                <div class="form-group col-md-9">
                                                     <label for="name">{{ trans('name') }} *</label>
                                                     <input required type="text" class="form-control" name="{{ $lang }}[name]" value="{{ $location->translate($lang)->name }}" id="{{ $lang }}[name]">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    
                                    <div class="form-group  row">
                                        <div class="form-group col-md-9">
                                            <div class="form-group overflow-hidden">
                                                <label for="code_id">{{trans('code')}}</label>
                                                <input type="text" required class="form-control" name="code_id" id="code_id" value="{{old("code_id",  $location->code_id)}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group  row">
                                        <div class="form-group col-md-9">
                                            <div class="form-group overflow-hidden">
                                                <label for="route_id">{{trans('route')}}</label>
                                                <select required name="route_id"  class="form-control select2 w-100" id="route_id">
                                                    <option value="">{{ trans('select_route') }}</option>
                                                    @foreach($routes as $route)
                                                        <option value="{{ $route->id}}" {{ old("route_id", $location->route_id) == $route->id ? "selected":null }}>{{ $route->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" {{ $location->active ? 'checked' : '' }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">Active</span>
                                        </label>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
