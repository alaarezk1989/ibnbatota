@extends('admin.layout')

@section('content')
<?php
use \App\Constants\ClassTypes;
?>
    <div class="app-content">
        <section class="section">
            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{ trans('locations')." ( ".trans('zones'). " ) "  }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">{{ trans('home') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.locations.index') }}" class="text-light-color">{{ trans('locations') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ trans('new') }}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ trans('new_location')." ( ".trans('zones'). " ) "  }}</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')
                                <form action="{{ route('admin.locations.store') }}" method="post">
                                    @csrf
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $lang == app()->getLocale() ? 'active': ''}} show" id="home-tab2" data-toggle="tab" href="#lang-{{ $lang }}" role="tab" aria-controls="home" aria-selected="true">{{ trans($language) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <div class="tab-pane fade {{ $lang == app()->getLocale() ? 'active show': ''}}" id="lang-{{ $lang }}" role="tabpanel" aria-labelledby="home-tab2">
                                                <div class="form-group col-md-9">
                                                    <label for="name">{{ trans('name') }} *</label>
                                                    <input required type="text" class="form-control" name="{{ $lang }}[name]"  id="{{ $lang }}[name]" value="{{ old($lang.'.name') }}">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    
                                    <div class="form-group  row">
                                        <div class="form-group col-md-9">
                                            <div class="form-group overflow-hidden">
                                                <label for="code_id">{{trans('code')}}</label>
                                                <input type="text" required name="code_id" class="form-control" id="code_id" value="{{old("code_id")}}">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group  row">
                                        <div class="form-group col-md-9">
                                            <div class="form-group overflow-hidden">
                                                <label for="route_id">{{trans('route')}}</label>
                                                <select required name="route_id"  class="form-control select2 w-100" id="route_id">
                                                    <option value="">{{ trans('select_route') }}</option>
                                                    @foreach($routes as $route)
                                                        <option value="{{ $route->id}}" {{ old("route_id") == $route->id ? "selected":null }}>{{ $route->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" checked class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{ trans('active') }}</span>
                                        </label>
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{ trans('save') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAndFqevHboVWDN156vJqXk1Y1-D7QR7BE&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    $('#city_id').on('change', function() {
        var lang = '{{ app()->getLocale() }}';
        var l = 0;
        if (lang == 'en') {
            l = 1;
        }
        $.ajax({
            url: '{{ route("api.locations.index") }}',
            type: 'get',
            data: { _token: '{{ csrf_token() }}', 'city_id' : this.value},
            success: function(data){
                $('#div_region').css('visibility','visible');
                var html='<option value ="">{{ trans("select_region") }}</option>';
                var i;
                for(i = 0; i < data.length; i++) {
                    html+= '<option value ="'+data[i].id+'">'+data[i].translations[l].name+'</option>';
                }
                $('#region_id').html(html);

                if ($('#city_id').val() === "") {
                    $('#div_region').css('visibility','hidden');
                }
            },
            error: function(){
                alert("error");
            }
        });
    });
</script>
@endsection
