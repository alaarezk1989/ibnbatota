<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <div class="dropdown user-pro-body text-center">
            <div class="nav-link pl-1 pr-1 leading-none ">
                <img src="{{ asset( auth()->user()->image) }}" alt="user-img"
                     class="avatar-xl rounded-circle mb-1">
                <span class="pulse bg-success" aria-hidden="true"></span>
            </div>
            <div class="user-info">
                <h6 class=" mb-1 text-dark">{{auth()->user()->user_name}}</h6>
                <span class="text-muted app-sidebar__user-name text-sm">{{ auth()->user()->email }} </span>
            </div>
        </div>
    </div>
    <ul class="side-menu">
        <li>
            <a class="side-menu__item" href="{{ route('admin.home.index') }}">
                <i class="side-menu__icon fa fa-area-chart"></i>
                <span class="side-menu__label">{{ trans('dashboard') }}</span>
            </a>
        </li>
        @can("index", Route::class)
            <li>
                <a class="side-menu__item" href="{{ route('admin.routes.index') }}">
                    <span><i class="side-menu__icon fa fa-building"></i>{{ trans('routes') }}</span>
                </a>
            </li>
        @endcan
      
        @can("index", Product::class)
            <li>
                <a class="slide-item" href="{{ route('admin.products.index') }}">
                    <span><i class="side-menu__icon fa fa-shopping-cart"></i>{{ trans('products') }}</span>
                </a>
            </li>
        @endcan

        @can("index", Order::class)
            <li>
                <a class="slide-item" href="{{ route('admin.orders.index') }}">
                    <span><i class="side-menu__icon fa fa fa-money"></i>{{ trans('orders') }}</span>
                </a>
            </li>
        @endcan
        
        @can("index", \App\Models\Category::class)
            <li>
                <a class="slide-item" href="{{ route('admin.categories.index') }}">
                    <span><i class="side-menu__icon fa fa-tags"></i>{{ trans('categories') }}</span>
                </a>
            </li>
        @endcan

                @can("index", User::class)
                    <li>
                        <a class="slide-item" href="{{ route('admin.users.index') }}">
                            <span><i class="side-menu__icon fa fa-user"></i>{{ trans('users') }}</span>
                        </a>
                    </li>
                @endcan

                @can("index" , Group::class)
                    <li>
                        <a class="slide-item" href="{{ route('admin.groups.index') }}">
                            <span><i class="side-menu__icon fa fa-group"></i>{{ trans('groups') }}</span>
                        </a>
                    </li>
                @endcan
                @can("index", Permission::class)
                    <li>
                        <a class="slide-item" href="{{ route('admin.permissions.index') }}">
                            <span><i class="side-menu__icon fa fa-key"></i>{{ trans('permissions') }}</span>
                        </a>
                    </li>
                @endcan
        @can("index", Location::class)
            <li>
                <a class="side-menu__item" href="{{ route('admin.locations.index') }}">
                    <span><i class="side-menu__icon fa fa-map-pin"></i>{{ trans('locations')."( ".trans('zones'). ")"  }}</span>
                </a>
            </li>
        @endcan
        {{-- @can("index", Setting::class)
            <li>
                <a class="side-menu__item" href="{{ route('admin.settings.index') }}">
                    <span><i class="side-menu__icon fa fa-cogs"></i>{{ trans('settings') }}</span>
                </a>
            </li>
        @endcan --}}
       
       
        @can("index", Log::class)
            <li>
                <a class="side-menu__item" href="{{ route('admin.logs.index') }}">
                    <span><i class="side-menu__icon fa fa-pie-chart"></i>{{ trans('logs') }}</span>

                </a>
            </li>
        @endcan
       
    </ul>
</aside>
