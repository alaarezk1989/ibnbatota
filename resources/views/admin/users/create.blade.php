@extends('admin.layout')

@section('content')
<?php
use \App\Constants\GenderTypes;
use \App\Constants\UserTypes;
use \App\Constants\ClassTypes;
?>
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{trans('users')}}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}"
                                                   class="text-light-color">{{trans('home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}"
                                                   class="text-light-color">{{trans('users')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{trans('new')}}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{trans('new_user')}}</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')
                                <form action="{{ route('admin.users.store') }}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf

                                    <div class=" row">

                                        <div class="form-group col-md-6">
                                            <label for="name">{{trans('name')}} *</label>
                                            <input required type="text" class="form-control" name="name" id="name"
                                                   value="{{ old('name') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="email">{{trans('email')}}</label>
                                            <input required type="email" class="form-control" name="email" id="email"
                                                   value="{{ old('email') }}">
                                        </div>
                                        
                                    </div>

                                    <div class=" row">
                                        <div class="form-group col-md-4">
                                            <label for="phone">{{trans('phone')}} *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input required type="text" class="form-control" name="phone" id="phone"
                                                       value="{{ old('phone') }}" data-inputmask='"mask": "99999999999"'
                                                       data-mask>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="password">{{trans('password')}} *</label>
                                            <input type="password" class="form-control" name="password" id="password"  >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="password_confirmation">{{trans('password_confirmation')}} *</label>
                                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" >
                                        </div>
                                    </div>

                                    <div class=" row">
                                        <div class="form-group col-md-4">
                                            <div class="form-group overflow-hidden">
                                                <label for="route_id">{{trans('route')}}</label>
                                                <select required name="route_id" id="route_id"  class="form-control select2 w-100" style="width: 100%">
                                                    <option value="" disabled>{{ trans('select_route') }}</option>
                                                    @foreach($routes as $route)
                                                        <option value="{{ $route->id }}" {{ old('route_id') == $route->id ? "selected": null }}>{{ $route->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4 " id="locations" style="visibility: hidden">
                                            <div class="form-group overflow-hidden">
                                                <label for="company_id">{{trans('location')}}</label>
                                                <select required name="location_id" id="location_id"  class="form-control select2 w-100" style="width: 100%">
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <div class="form-group overflow-hidden">
                                                <label for="types">{{trans('user_type')}} *</label>
                                                <select required name="type" class="form-control select2 w-100" id="types" style="width: 100%">
                                                    <option value="">{{ trans('select_user_type') }}</option>
                                                    @foreach(UserTypes::getList() as $key => $value)
                                                        <option value="{{ $key }}" {{ old('type') == $key ? "selected":null }}>{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=" row">
                                        <div class="col-md-4">
                                            <label class="custom-switch">
                                                <input type="checkbox" name="status" value="1" checked class="custom-switch-input">
                                                <span class="custom-switch-indicator"></span>
                                                <span class="custom-switch-description">{{trans('status')}}</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <div class="form-group col-md-3">
                                            <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5">
                                                <i class="fa fa-save"></i> {{trans('save')}}
                                            </button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>
    </div>
@stop

@section('scripts')
    <script>
                
        /****** to preview uploaded image ******/
        function readURL(input, img_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var image_id=$('#' + img_id);
                reader.onload = function (e) {
                    image_id.attr('src', e.target.result);
                };
                image_id.css("width", "150px");
                image_id.css("height", "150px");
                reader.readAsDataURL(input.files[0]);
            }
        }
/****************************************/
    </script>
    <script>
        $('#route_id').on('change', function() {
            var route =  this.value;
            $.ajax({
                    url: '{{ route("getRouteLocations") }}',
                    type: 'get',
                    data: { _token: '{{ csrf_token() }}', 'route':route},
    
                    success: function(data){
                        document.getElementById('locations').style.visibility = 'visible';
                        var html='<option value ="">{{ trans("select_location") }}</option>';
                        var i;
                        for(i=0;i<data.length;i++){
                            html+=
                            '<option value ="'+data[i].id+'">'+data[i].name+'</option>';
                        }
                        $('#location_id').html(html);
                    },
                    error: function(){
                        alert("error");
                    }
            });
        });
    </script>
@stop
