@extends('admin.layout')

@section('content')
<?php

use \App\Constants\GenderTypes;
use \App\Constants\UserTypes;
use \App\Constants\ClassTypes;
?>
<div class="app-content">
    <section class="section">

        <!--page-header open-->
        <div class="page-header">
            <h4 class="page-title">{{trans('users')}}</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">{{trans('home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}" class="text-light-color">{{trans('users')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('update_user')}}</li>
            </ol>
        </div>
        <!--page-header closed-->

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>{{trans('update_user')}}</h4>
                        </div>
                        <div class="card-body">
                            @include('admin.errors')
                            <form action="{{ route('admin.users.update', ['user' => $user]) }}" method="Post" enctype="multipart/form-data" autocomplete="off" >
                                @method('PUT')
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id}}" />


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="name">{{trans('name')}} *</label>
                                        <input required type="text" class="form-control" name="name" id="name" value="{{ !(old('name'))? $user->name : old('name' )}}" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">{{trans('email')}} </label>
                                        <input required type="email" class="form-control" name="email" id="email"  value="{{ !(old('email'))? $user->email : old('email') }}" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="phone">{{trans('phone')}} *</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input required type="text" class="form-control"  name="phone" id="phone"  value="{{ !(old('phone'))? $user->phone : old('phone') }}" data-inputmask='"mask": "99999999999"' data-mask >
                                        </div>
                                    </div>
                            
                                    <div class="form-group col-md-4">
                                        <label for="password">{{trans('password')}}</label>
                                        <input type="password" class="form-control" name="password" id="password"  >
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="password_confirmation">{{trans('password_confirmation')}}</label>
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4" >
                                        <div class="form-group overflow-hidden">
                                            <label for="route_id">{{trans('route')}}</label>
                                            <select required name="route_id"  class="form-control select2 w-100" id="route_id" style="width: 100%">
                                                <option value="">{{ trans('select_route') }}</option>
                                                @foreach($routes as $route)
                                                    <option value="{{ $route->id }}" {{ $user->route_id == $route->id ? "selected":null }}>{{ $route->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4"  id="locations">
                                        <div class="form-group overflow-hidden">
                                            <label for="location_id">{{trans('location')}}</label>
                                            <select id="location_id" required name="location_id" class="form-control select2 w-100"  style="width: 100%">
                                                @foreach($locations as $location)
                                                    <option value="{{ $location->id }}"   @if($user->location_id == $location->id) selected @endif>{{ $location->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="form-group overflow-hidden">
                                            <label for="types">{{trans('user_type')}} *</label>
                                            <select required name="type" class="form-control select2 w-100" id="types" style="width: 100%">
                                                <option value="">{{ trans('select_user_type') }}</option>
                                                @foreach(UserTypes::getList() as $key => $value)
                                                    <option value="{{ $key }}" {{ old('type', $user->type) == $key ? "selected":null }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="status" value="1" {{ $user->status ? 'checked' : '' }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{trans('status')}}</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{trans('save')}}</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </section>
</div>
@stop
@section('scripts')
<script>
  $('#route_id').on('change', function() {
            var route =  this.value;
            $.ajax({
                    url: '{{ route("getRouteLocations") }}',
                    type: 'get',
                    data: { _token: '{{ csrf_token() }}', 'route':route},
    
                    success: function(data){
                        document.getElementById('locations').style.visibility = 'visible';
                        var html='<option value ="">{{ trans("select_location") }}</option>';
                        var i;
                        for(i=0;i<data.length;i++){
                            html+=
                            '<option value ="'+data[i].id+'">'+data[i].name+'</option>';
                        }
                        $('#location_id').html(html);
                    },
                    error: function(){
                        alert("error");
                    }
            });
        });
</script>
@stop
