@extends('admin.layout')

@section('content')
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{trans('products')}}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">{{trans('home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.products.index') }}" class="text-light-color">{{trans('products')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{trans('edit')}}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{trans('edit_product')}}</h4>
                            </div>

                            @if(session()->has('success'))
                                <div class="alert alert-success alert-has-icon alert-dismissible show fade">
                                    <div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
                                    <div class="alert-body">
                                        <button class="close" data-dismiss="alert">
                                            <span>×</span>
                                        </button>
                                        <div class="alert-title">{{trans('success')}}</div>
                                        {{ session('success') }}
                                    </div>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-has-icon alert-dismissible show fade">
                                    <div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
                                    <div class="alert-body">
                                        <button class="close" data-dismiss="alert">
                                            <span>×</span>
                                        </button>
                                        <div class="alert-title">{{trans('error')}}</div>
                                        {{ session('error') }}
                                    </div>
                                </div>
                            @endif

                            <div class="card-body">
                                @include('admin.errors')
                                <form action="{{ route('admin.products.update', ['product' => $product]) }}" enctype="multipart/form-data" method="post">
                                    @method('PUT')
                                    @csrf
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $lang == app()->getLocale() ? 'active': ''}} show" id="home-tab2" data-toggle="tab" href="#lang-{{ $lang }}" role="tab" aria-controls="home" aria-selected="true">{{ trans($language) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">

                                        @foreach(config()->get('app.locales') as $lang => $language)
                                        <div class="tab-pane fade {{ $lang == app()->getLocale() ? 'active show': ''}}" id="lang-{{ $lang }}" role="tabpanel" aria-labelledby="home-tab2">
                                            <div class="form-group col-md-9">
                                                <label for="name">{{trans('name')}} *</label>

                                                <input required type="text" class="form-control" value="{{ !old( $lang.'.name') ? $product->translate($lang)->name : old( $lang.'.name') }}" name="{{ $lang }}[name]" id="name" >
                                            </div>

                                            <div class="form-group col-md-9">
                                                <label for="description">{{trans('description')}} *</label>
                                                <textarea required class="form-control" rows="9" cols="50" name="{{ $lang }}[description]" id="description" >{{ !old($lang.".description") ? $product->translate($lang)->description : old($lang.".description") }}</textarea>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-md-9">
                                        <div class="form-group overflow-hidden">
                                            <label for="categories">{{trans('categories')}} *</label>
                                            <select name="category_id" required class="form-control select2 w-100" id="categories" >
                                                <option value="">{{ trans('select_category') }}</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" {{ (old("category_id") == $category->id or $product->category_id == $category->id) ? "selected" : null }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="code">{{trans('code')}}</label>
                                        <input type="text" required class="form-control" value="{{ old("code", $product->code )  }}" name="code" id="code" >
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="price">{{trans('price')}} *</label>
                                        <input type="number" required class="form-control" value="{{ old("price", $product->price ) }}" name="price" id="price" >
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="discount">{{trans('discount' )}} *</label>
                                        <input type="number" class="form-control" value="{{ old("discount", $product->discount ) }}" name="discount" id="discount" >
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                        <br><br>
                                        <label class="custom-switch">
                                            {{-- <input type="hidden" id="default" name="icon" value="{{ $product->icon }}"> --}}
                                            <input type="file" onchange="readURL(this,'upload_img');" name="image" class="icon" style="visibility: hidden;">
                                            <img id="upload_img" src="{{ asset($product->image) }}" width="300px;height:150px">
                                        </label>
                                    </div>
                                 
                                    <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" {{ old("active",$product->active) == 1 ? "checked" : null }}  class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{trans('active')}}</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{trans('save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
<script>
    /****** to preview uploaded icon ******/
    function readURL(input, img_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var icon_id=$('#' + img_id);
            reader.onload = function (e) {
                icon_id.attr('src', e.target.result);
            };
            icon_id.css("width", "260px");
            icon_id.css("height", "261px");
            reader.readAsDataURL(input.files[0]);
            $("#default").remove();
        }
    }
</script>
