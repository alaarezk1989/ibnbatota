@extends('admin.layout')

@section('content')
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{trans('products')}}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">{{trans('home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.products.index') }}" class="text-light-color">{{trans('products')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{trans('new')}}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{trans('new_product')}}</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')
                                <form action="{{ route('admin.products.store') }}" enctype="multipart/form-data" method="post">
                                    @csrf
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $lang == app()->getLocale() ? 'active': ''}} show" id="home-tab2" data-toggle="tab" href="#lang-{{ $lang }}" role="tab" aria-controls="home" aria-selected="true">{{ trans($language) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">

                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <div class="tab-pane fade {{ $lang == app()->getLocale() ? 'active show': ''}}" id="lang-{{ $lang }}" role="tabpanel" aria-labelledby="home-tab2">
                                                <div class="form-group col-md-9">
                                                    <label for="name">{{trans('name')}} *</label>

                                                    <input type="text" required class="form-control" value="{{ old( $lang.'.name' ) }}" name="{{ $lang }}[name]" id="name" >
                                                </div>
                                                <div class="form-group col-md-9">
                                                    <label for="description">{{trans('description')}} *</label>
                                                    <textarea class="form-control" required rows="9" cols="50" name="{{ $lang }}[description]" id="description" >{{ old($lang.".description") }}</textarea>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-md-9">
                                        <div class="form-group overflow-hidden">
                                            <label for="categories">{{trans('categories')}} *</label>
                                            <select name="category_id"  required class="form-control select2 w-100" id="units" >
                                                <option value="">{{ trans('select_category') }}</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" {{ old("category_id") == $category->id ? "selected":null }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="code">{{trans('code')}} *</label>
                                        <input type="text" required class="form-control" value="{{ old("code") }}" name="code" id="code" >
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="price">{{trans('price')}} *</label>
                                        <input type="number" required class="form-control" value="{{ old("price") }}" name="price" id="price" >
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="discount">{{trans('discount')}} *</label>
                                        <input type="number" class="form-control" value="{{ old("discount") }}" name="discount" id="discount" >
                                    </div>
                                   <div class="form-group col-md-9">
                                        <br><br>
                                        <label for="custom-switch">
                                            <input type="file" name="image" onchange="readURL(this,'upload_img');" name="image" class="icon" >
                                            <img id="upload_img" src="{{ asset('assets/img/add_img.jpg') }}">                                            
                                        </label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" {{ old("active") == 1 ? "checked" : null }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{trans('active')}}</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{trans('save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
<script>
    /****** to preview uploaded icon ******/
    function readURL(input, img_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var icon_id=$('#' + img_id);
            reader.onload = function (e) {
                icon_id.attr('src', e.target.result);
            };
            icon_id.css("width", "260px");
            icon_id.css("height", "261px");
            reader.readAsDataURL(input.files[0]);
            $("#default").remove();
        }
    }
</script>